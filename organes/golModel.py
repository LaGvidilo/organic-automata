#coding: utf-8
import os

class Generator(object):
	def __init__(self, size=(5,5), DIRMODEL="gol_models/"):
		self.size = size
		self.zone = [0] * (self.size[0] * self.size[1])
		self.DIR_MODELS = DIRMODEL

	def generate(self):
		ID = 0
		while True:
			ID+=1
			self.zone[0]+=1
			i=0
			for s in self.zone[:-1]:
				if s > 1:
					self.zone[i] = 0
					self.zone[i+1]+=1
				i+=1
			DATA = {'size':self.size, 'zone':self.zone, 'id':ID}
			f = open(self.DIR_MODELS+str(ID)+"_"+str(self.size[0])+"x"+str(self.size[0])+"_cells.model","w")
			f.write(str(DATA))
			f.close()
			if self.zone == [1] * (self.size[0] * self.size[1]): break


for i in range(4,7):
	print("STEP: ",i)
	os.mkdir("/media/datas/gol_models/"+str(i)+"x"+str(i))
	g = Generator((i,i),"/media/datas/gol_models/"+str(i)+"x"+str(i)+"/")
	g.generate()