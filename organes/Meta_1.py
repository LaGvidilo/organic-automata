#coding: utf-8

from GOL import *

class M1(object):
	def __init__(self,imagefilepath,maxdivisions=64,mode="RGB"):
		self.maxdivisions = maxdivisions
		self.imagefilepath = imagefilepath
		image_file = Image.open(imagefilepath)
		self.size = image_file.size
		self.g = gol(image_file.size)
		for y in range(0,image_file.size[1]):
			for x in range(0,image_file.size[0]):
				if mode == "RGB":
					if image_file.getpixel((x,y)) != (0,0,0):
						self.g.setter(x,y)
				if mode == "1":
					if image_file.getpixel((x,y)) != 0:
						self.g.setter(x,y)
				if mode == "RGBA":
					if image_file.getpixel((x,y)) != (0,0,0,0):
						self.g.setter(x,y)

	def life(self,nb_gen=300,bordersize=1,display=False):
		borders = {'up':0, 'down':0, 'left':0, 'right':0}
		for r in range(0,nb_gen):
			print((r/(1.0*nb_gen))*100.0,"%(GENERATION #"+str(r)+")")
			if display: self.g.display()
			self.g.turn()
			up,down,left,right = self.get_border_percent(bordersize)
			borders['up'] += up
			borders['down'] += down
			borders['left'] += left
			borders['right'] += right
		return borders

	def get_border_percent(self,bordersize=1):
		plan,posindx = self.g.get_intern()
		
		border1 = []
		border2 = []
		for x in range(0,self.size[0]):
			for n in range(0,bordersize):
				border1.append(plan[posindx[x][n]])
				border2.append(plan[posindx[x][self.size[1]-n-1]])

		border3 = []
		border4 = []
		for y in range(0,self.size[1]):
			for n in range(0,bordersize):
				border3.append(plan[posindx[n][y]])
				border4.append(plan[posindx[self.size[0]-n-1][y]])

		cnt1 = 0
		for i in border1:
			if i == 1:
				cnt1+=1
		percent_up = (cnt1/(len(border1)*1.00))
		
		cnt2 = 0
		for i in border2:
			if i == 1:
				cnt2+=1
		percent_down = (cnt2/(len(border2)*1.00))

		cnt3 = 0
		for i in border3:
			if i == 1:
				cnt3+=1
		percent_left = (cnt3/(len(border3)*1.00))

		cnt4 = 0
		for i in border4:
			if i == 1:
				cnt4+=1
		percent_right = (cnt4/(len(border4)*1.00))
		print("percents:",(percent_up, percent_down, percent_left, percent_right))
		return (percent_up, percent_down, percent_left, percent_right)


