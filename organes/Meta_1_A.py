#coding: utf-8

from Meta_1 import *
import os

class M1A(object):
	def __init__(self, sizeworld=(128,128),filepathnewfolder="gol_img/world1/"):
		os.mkdir(filepathnewfolder)
		self.filepathnewfolder = filepathnewfolder
		self.world = {}
		self.sizeworld = sizeworld
		for x in range(0,sizeworld[0]):
			for y in range(0,sizeworld[1]):
				if x not in self.world.keys(): 
					self.world[x] = {}
				self.world[x][y] = []
		self.itt = 0

	def born_new(self,posx,posy,imagepath,maxdivisions=64,mode="RGB"):
		meta1 = M1(imagepath,maxdivisions,mode)
		self.world[posx][posy] = [meta1]

	def life(self,nb_gen=300,maxdivisions=64, bordersize=1, percentfordivise=0.10,display=False):
		for x in range(0,self.sizeworld[0]):
			for y in range(0,self.sizeworld[1]):
				for kzone in self.world[x][y]:
					borders = kzone.life(nb_gen=nb_gen,bordersize=bordersize,display=display)
					imgpath = kzone.imagefilepath
					if kzone.maxdivisions > 0:
						kzone.maxdivisions -= 1
						if borders['right'] >= percentfordivise:
							#print("BORDER OK")
							meta1 = M1(imgpath,maxdivisions)
							for xprime in range(x, self.sizeworld[0]-x):
								if self.world[xprime][y] == []:
									self.world[xprime][y] = [meta1]
						if borders['left'] >= percentfordivise:
							#print("BORDER OK")
							meta1 = M1(imgpath)
							for xprime in range(x, 0):
								if self.world[xprime][y] == []:
									self.world[xprime][y] = [meta1]
						if borders['up'] >= percentfordivise:
							#print("BORDER OK")
							meta1 = M1(imgpath)
							for yprime in range(y, 0):
								if self.world[x][yprime] == []:
									self.world[x][yprime] = [meta1]
						if borders['down'] >= percentfordivise:
							#print("BORDER OK")
							meta1 = M1(imgpath)
							for yprime in range(y, self.sizeworld[1]-y):
								if self.world[x][yprime] == []:
									self.world[x][yprime] = [meta1]

	def display(self):
		self.itt += 1
		image = Image.new("RGB", self.sizeworld)
		for x in range(0,self.sizeworld[0]):
			for y in range(0,self.sizeworld[1]):
				if len(self.world[x][y]) > 0:
					image.putpixel((x,y), (255,255,255))
		image.save(self.filepathnewfolder+"world_gen_"+str(self.itt)+".png", "PNG")

