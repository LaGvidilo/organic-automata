#coding: utf-8

from Meta_1_A import *


def convertimg(pathfile):
	image_file = Image.open(pathfile) # open colour image
	image_file = image_file.convert('L') # convert image to black and white
	image_file.save(pathfile.split(".")[0]+"_result.png")
	return pathfile.split(".")[0]+"_result.png"

run1A = M1A( (128,128) )
run1A.born_new(posx=64,posy=64,imagepath="assets/1-1.png",mode="1")
for i in range(0,500):
	print("STEP:",i)
	run1A.life(nb_gen=4, maxdivisions=256, bordersize=256, percentfordivise=0.001, display=True)
	run1A.display()